from django.shortcuts import render
from .models import *

from django.http import HttpResponseRedirect

def home(request):
    jasa = ProductJasa.objects.all()
    return render(
        request, 'home.html', {'jasa':jasa})

def addJasa(request):
    name = request.POST.get('name')
    descrisption = request.POST.get('descrisption')
    harga = request.POST.get('harga')
    stock = request.POST.get('stock')
    image = request.POST.get('image')
    category = request.POST.get('category')
    rating = request.POST.get('rating')

    jasa = ProductJasa(
        name = name,
        descrisption = descrisption,
        harga = harga,
        stock = stock,
        image = image,
        category = category,
        rating = rating,
        )
    
    jasa.save()

    return HttpResponseRedirect('home')