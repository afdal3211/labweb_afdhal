from django.db import models

class ProductCategory(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class ProductJasa(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    harga = models.PositiveIntegerField()
    stock = models.PositiveIntegerField()
    image = models.ImageField(upload_to='media/')
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    rating = models.IntegerField()

    def __str__(self):
        return self.name
