import './style.css'
import { Link } from 'react-router-dom';

function nav(){
    return(
        <div>
            <ul className="nav-selection">
                <link to="/resep"><li>Home</li></link>
                <link to="/create/resep"><li>Create</li></link>
            </ul>
        </div>
    )
}

export default nav;