import './App.css';
import React from 'react';
import { Routes, Route, Link } from "react-router-dom";
import ResepList from './components/resep-list';
import Nav from './components/nav';
import Resep from './pages/resep';
import CreateResep from './pages/create-resep';

function App() {
  return (
    <div>
      <Route>
        <Route path="/resep" element={<Resep />} />
        <Route path="/create/resep" element={<CreateResep />} />
      </Route>
    </div>
  );
}

export default App;
