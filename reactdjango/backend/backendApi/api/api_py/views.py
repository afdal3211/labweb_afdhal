from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import ApiModelSerializer2, apiModelSerializer
from .. import models

@api_view(['GET', 'POST'])
def api_list(request):
    if request.method == 'GET':
        qs = models.resepMakanan.objects.all()
        serializer = apiModelSerializer(qs, many=True)
        return Response(serializer.data)

    elif request.methode == 'POST':
        serializer = apiModelSerializer(data=request.data)
        if serializer.is_valid():
            tipe = models.tipeMakanan.objects.get(data=request.data)
            serializer.save(tipe=tipe)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Respone(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def api_list2(request):
    if request.method == 'GET':
        qs = models.tipeMakanan.objects.all()
        serializer = apiModelSerializer(qs, many=True)
        return Response(serializer.data)

    elif request.methode == 'POST':
        serializer = ApiModelSerializer2(data=request.data)
        if serializer.is_valid():
            tipe = models.tipeMakanan.objects.get(data=request.data)
            serializer.save(tipe=tipe)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Respone(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

