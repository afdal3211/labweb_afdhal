from django.db import models

# Create your models here.
class Universitas(models.Model):
    Nama_Universitas = models.CharField(max_length=100)
    Alamat = models.CharField(max_length=50)
    Gambar = models.ImageField(upload_to='image/')

    def __str__(self):
        return self.Nama_Universitas

class ProgramStudi(models.Model):
    Prodi = models.CharField(max_length=100)
    Ketua_Prodi = models.CharField(max_length=50)
    Universitas_Prodi = models.ForeignKey(Universitas, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.Prodi

class Dosen(models.Model):
    Nama_Dosen = models.CharField(max_length=100)
    Email = models.EmailField(max_length=50)
    Phone = models.CharField(max_length=12)
    Prodi = models.ForeignKey(ProgramStudi, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.Nama_Dosen

class Mahasiswa(models.Model):
    Nama_Mahasiswa = models.CharField(max_length=100)
    Nim = models.CharField(max_length=50, default=None)
    Email = models.EmailField(max_length=50)
    Phone = models.CharField(max_length=12)
    Prodi_Mahasiswa = models.ForeignKey(ProgramStudi, on_delete=models.CASCADE)

    def __str__(self):
        return self.Nama_Mahasiswa