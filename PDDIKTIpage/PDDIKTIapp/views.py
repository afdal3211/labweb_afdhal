from django.shortcuts import render
from .models import *
from django.http import HttpResponseRedirect


# Create your views here.
def data_view(request):
    mahasiswa = Mahasiswa.objects.all()
    context = {'mahasiswa':mahasiswa}
    return render(request, 'data.htm', context)
   

def add_mahasiswa(request):

    if request.method == 'POST':
        Nama_Mahasiswa = request.POST.get('Nama_Mahasiswa')
        Nim = request.POST.get('Nim')
        Prodi_id = request.POST.get('Prodi_Mahasiswa')
        Prodi_Mahasiswa = ProgramStudi.objects.get(id=Prodi_id)
        Email = request.POST.get('Email')
        Phone = request.POST.get('Phone')

        Mahasiswa.objects.create(
            Nama_Mahasiswa=Nama_Mahasiswa, 
            Nim=Nim,
            Prodi_Mahasiswa=Prodi_Mahasiswa,
            Email=Email,
            Phone=Phone,
        )
        return HttpResponseRedirect('/data/')

    return render(request, 'form.htm', {
            'Prodis':ProgramStudi.objects.all(),
            })


def search_siswa(request):
    if request.method == 'POST':
        nama = request.POST.get('name')
        mahasiswa = Mahasiswa.objects.filter(Nama_Mahasiswa__contains=nama)
        context = {'mahasiswa':mahasiswa, 'nama':nama}

        return render(request, 'search.htm' ,context)
