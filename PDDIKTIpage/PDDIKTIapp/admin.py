from django.contrib import admin
from .models import Mahasiswa, Dosen, Universitas, ProgramStudi

# Register your models here.
admin.site.register(Mahasiswa)
admin.site.register(Dosen)
admin.site.register(Universitas)
admin.site.register(ProgramStudi)