from django.apps import AppConfig


class PddiktiappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PDDIKTIapp'
