from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from . import views

urlpatterns = [
    # path('', views.home, name="home"),
    path('', views.leaderboard_view, name="leaderboard_view"),
    path('login/', views.home, name="home"),
    path('question/', views.question, name="question"),
    path('result/', views.question),
    path('add/', views.addQuestion, name='add'),
    path('add1/', views.question1, name='add1'),
]