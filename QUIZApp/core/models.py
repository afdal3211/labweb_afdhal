from django.db import models

# Create your models here.

class QuestionModel(models.Model):
    question = models.CharField(max_length=200,null=True)
    option1 = models.CharField(max_length=200,null=True)
    option2 = models.CharField(max_length=200,null=True)
    option3 = models.CharField(max_length=200,null=True)
    option4 = models.CharField(max_length=200,null=True)
    option5 = models.CharField(max_length=200,null=True)
    answer = models.CharField(max_length=200,null=True)
    
    def __str__(self):
        return self.question

class Score(models.Model):
    nama = models.CharField(max_length=255, default=None)
    score = models.IntegerField()

    def __str__(self):
        return self.nama + self.score


class DaftarNama(models.Model):
    test = models.CharField(max_length=100)
    nama = models.ForeignKey(Score, on_delete=models.CASCADE)
    hewans = (
    ('Darat', 'Darat'),
    ('Air', 'Air'),
    )
    hewan = models.CharField(choices=hewans,max_length=100)

    def __str__(self):
        return self.test