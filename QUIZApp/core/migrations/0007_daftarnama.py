# Generated by Django 3.2 on 2022-12-08 06:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_delete_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='DaftarNama',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test', models.CharField(max_length=100)),
                ('hewan', models.CharField(choices=[('Darat', 'Darat'), ('Air', 'Air')], max_length=100)),
                ('nama', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.score')),
            ],
        ),
    ]
