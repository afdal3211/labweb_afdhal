from django.shortcuts import render
from .models import *
from django.http import HttpResponse

# Create your views here.
def home(request):
    return render(request, 'login.html')
    # username = request.POST.get('username')
    # name = User(user=username)
    # name.save()
    # return HttpResponse('/quiz/')

def leaderboard_view(request):
    leaderboard = Score.objects.all().order_by('-score').values()
    return render(request, 'leaderboard.html', {'leaderboard':leaderboard})

# def insert(request):
#     username = request.POST.get('username')
#     name = User(user=username)
#     name.save()
#     return HttpResponse('/quiz/')

def question(request):
    if request.method == 'POST':
        print(request.POST)
        questions=QuestionModel.objects.all()
        score=0
        wrong=0
        correct=0
        total=0
        nama = request.POST.get('nama')
        totalscore = QuestionModel.objects.count()
        for q in questions:
            total+=1
            answer = request.POST.get(q.question)
            items = vars(q)
            if q.answer == items[answer]:
                score+=(100/totalscore)
                correct+=1
            else:
                wrong+=1
        percent = score/(total*10) *100
        context = {
            'score':score,
            'correct':correct,
            'wrong':wrong,
            'percent':percent,
            'total':total,
            'nama':nama,
        }
        Score.objects.create(
            nama=nama, #var 1 dari model var 2 dari user input
            score=score,
        )
        return render(request,'result.html',context)
    else:
        questions=QuestionModel.objects.all()
        context = {
            'questions':questions
        }
        return render(request,'quiz.html',context)

def addQuestion(request):
    if request.method == 'POST':
        question = request.POST.get('question')
        option1 = request.POST.get('option1')
        option2 = request.POST.get('option2')
        option3 = request.POST.get('option3')
        option4 = request.POST.get('option4')
        option5 = request.POST.get('option5')
        answer = request.POST.get('answer')

        QuestionModel.objects.create(
            question=question,
            option1=option1,
            option2=option2,
            option3=option3,
            option4=option4,
            option5=option5,
            answer=answer,
        )

    return render(request, 'add.html')

def question1(request):
    score = Score.objects.all()
    list1 = DaftarNama.objects.all()

    if request.method == 'POST':
        test = request.POST.get('test')
        nama = request.POST.get('nama')
        hewan = request.POST.get('hewan')
        nama1 = Score.objects.get(nama=nama)

        DaftarNama.objects.create(
            test=test,
            nama=nama1,
            hewan=hewan,
        )
    

    return render(request, 'add1.html', {'list1':list1, 'score':score})